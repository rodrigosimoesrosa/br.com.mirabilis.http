package br.com.mirabilis.http.listener;

import br.com.mirabilis.http.HttpRequest;

/**
 * Interface of {@link HttpRequest}
 * 
 * @author Rodrigo Sim�es Rosa.
 * 
 * @param <T>
 */
public interface HttpRequestListener<T> {

	/**
	 * Method that result of request {@link HttpRequest}
	 * 
	 * @param data
	 */
	public void onResponseHttp(T data);
}
