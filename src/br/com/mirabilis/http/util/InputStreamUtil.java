package br.com.mirabilis.http.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

/**
 * Class that convert {@link InputStream} to same objects type { {@link String}
 * , {@link JSONObject} , and Array of bytes []}
 * 
 * @author rodrigosimoesrosa
 *
 */
public class InputStreamUtil {

	public static final String INPUT_STREAM_MESSAGE_EMPTY = "InputStream is empty!";

	/**
	 * Do convert {@link InputStream} to bytes array
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static byte[] toBytes(InputStream in) throws IOException {
		if (in == null) {
			throw new NullPointerException(INPUT_STREAM_MESSAGE_EMPTY);
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			byte[] buf = new byte[1024];
			int length;
			while ((length = in.read(buf)) > 0) {
				bos.write(buf, 0, length);
			}
			byte[] bytes = bos.toByteArray();
			return bytes;
		} finally {
			bos.close();
		}
	}

	/**
	 * Do convert {@link InputStream} to {@link JSONObject}
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	@SuppressWarnings("deprecation")
	public static JSONObject toJSON(InputStream in) throws IOException,
			JSONException {
		if (in == null) {
			throw new NullPointerException(INPUT_STREAM_MESSAGE_EMPTY);
		}

		BufferedReader reader;
		reader = new BufferedReader(new InputStreamReader(in, HTTP.UTF_8));
		StringBuilder stringBuilder = new StringBuilder();
		String temp;
		while ((temp = reader.readLine()) != null) {
			stringBuilder.append(temp);
		}
		return new JSONObject(stringBuilder.toString());
	}

	/**
	 * Do convert {@link InputStream} to {@link String}
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static String toString(InputStream in) throws IOException {
		if (in == null) {
			throw new NullPointerException(INPUT_STREAM_MESSAGE_EMPTY);
		}
		byte[] bytes = toBytes(in);
		String content = new String(bytes);
		return content;
	}

	/**
	 * Do convert {@link InputStream} to XML {@link String}
	 * 
	 * @param in
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static String toXML(InputStream in)
			throws ParserConfigurationException, SAXException, IOException {
		if (in == null) {
			throw new NullPointerException(INPUT_STREAM_MESSAGE_EMPTY);
		}
		return toDocumentXML(in).toString();
	}

	/**
	 * Do convert {@link InputStream} to {@link Document}
	 * 
	 * @param in
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document toDocumentXML(InputStream in)
			throws ParserConfigurationException, SAXException, IOException {
		if (in == null) {
			throw new NullPointerException(INPUT_STREAM_MESSAGE_EMPTY);
		}
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbFactory.newDocumentBuilder();
		return builder.parse(in);
	}
}
