package br.com.mirabilis.http.jakarta;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import br.com.mirabilis.http.HttpRequest;
import br.com.mirabilis.http.exception.HttpRequestException;
import br.com.mirabilis.http.info.ContentType;
import br.com.mirabilis.http.info.HeaderType;
import br.com.mirabilis.http.listener.HttpRequestListener;
import br.com.mirabilis.http.util.InputStreamUtil;

/**
 * Implementation of {@link HttpRequest} using Jakarta lib's for request http.
 * 
 * @author Rodrigo Sim�es Rosa
 * 
 */
@SuppressWarnings("deprecation")
public final class HttpJakarta extends HttpRequest {

	public HttpJakarta() {
	}

	public InputStream get(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return get(url, this.timeoutConnection, this.timeoutSocket);
	}

	@SuppressWarnings("resource")
	public InputStream get(String url, int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, ClientProtocolException, IOException {

		HttpClient httpClient = new DefaultHttpClient(getHttpParams(
				timeoutConnection, timeoutSocket));
		HttpGet httpGet = new HttpGet(url);
		HttpResponse httpResponse = null;
		HttpEntity httpEntity = null;

		InputStream data = null;

		httpResponse = httpClient.execute(httpGet);
		httpEntity = httpResponse.getEntity();

		int status = httpResponse.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			if (httpEntity != null) {
				data = httpEntity.getContent();
			} else {
				throw new HttpRequestException(ENTITY_NULL);
			}
		} else {
			throw new HttpRequestException(ERROR_HTTP + status);
		}
		return data;
	}

	public InputStream post(String url, Map<String, Object> map)
			throws ClientProtocolException, HttpRequestException, IOException {
		return post(url, map, this.timeoutConnection, this.timeoutSocket);
	}

	@SuppressWarnings("resource")
	public InputStream post(String url, Map<String, Object> map,
			int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, ClientProtocolException, IOException {

		InputStream data = null;

		HttpClient httpClient = new DefaultHttpClient(getHttpParams(
				timeoutConnection, timeoutSocket));
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> params = getParams(map);
		httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity entity = response.getEntity();
		if (entity == null) {
			throw new HttpRequestException(
					"HttpRequest.post response is equals null!");
		} else {
			data = entity.getContent();
		}
		return data;
	}

	@SuppressWarnings("resource")
	public JSONObject getJson(String url) throws ClientProtocolException,
			HttpRequestException, IOException, JSONException {
		JSONObject data = null;
		InputStream in = null;

		HttpClient httpClient = new DefaultHttpClient(getHttpParams(
				timeoutConnection, timeoutSocket));
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader(ContentType.getString(), ContentType.JSON.toString());
		httpGet.setHeader(HeaderType.ACCEPT.toString(),
				ContentType.JSON.toString());

		HttpResponse httpResponse = null;
		HttpEntity httpEntity = null;

		httpResponse = httpClient.execute(httpGet);
		httpEntity = httpResponse.getEntity();

		int status = httpResponse.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			if (httpEntity != null) {
				in = httpEntity.getContent();
				data = InputStreamUtil.toJSON(in);
			} else {
				throw new HttpRequestException(ENTITY_NULL);
			}
		} else {
			throw new HttpRequestException(ERROR_HTTP + status);
		}
		return data;
	}

	public void postXML(String url, String xml,
			HttpRequestListener<String> listener)
			throws ClientProtocolException, HttpRequestException, IOException,
			IllegalStateException, ParserConfigurationException, SAXException {
		postXML(url, xml, listener, this.cryptFormat, this.timeoutConnection,
				this.timeoutSocket);
	}

	@SuppressWarnings("resource")
	public void postXML(String url, String xml,
			HttpRequestListener<String> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, ClientProtocolException, IOException,
			IllegalStateException, ParserConfigurationException, SAXException {

		HttpClient httpClient = new DefaultHttpClient(getHttpParams(
				timeoutConnection, timeoutSocket));
		HttpPost httpPost = new HttpPost(url);
		HttpResponse httpResponse = null;
		HttpEntity httpEntity = null;

		String data = null;

		StringEntity stringEntity = new StringEntity(xml, cryptFormat);
		stringEntity.setContentType(ContentType.XML.toString());

		httpPost.setEntity(stringEntity);

		httpPost.addHeader(HeaderType.ACCEPT.toString(),
				ContentType.XML_APP.toString());
		httpPost.addHeader(HTTP.CONTENT_TYPE, ContentType.XML_APP.toString());

		httpResponse = httpClient.execute(httpPost);
		httpEntity = httpResponse.getEntity();
		int status = httpResponse.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			if (httpEntity != null) {
				data = InputStreamUtil.toXML(httpEntity.getContent());
				EntityUtils.toString(httpEntity);
			} else {
				throw new HttpRequestException(ENTITY_NULL);
			}
		} else {
			throw new HttpRequestException(ERROR_HTTP + status);
		}
		listener.onResponseHttp(data);
	}

	@SuppressWarnings("resource")
	public void postJson(String url, JSONObject json,
			HttpRequestListener<JSONObject> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws ClientProtocolException, IOException, HttpRequestException,
			JSONException {

		HttpClient httpClient = new DefaultHttpClient(getHttpParams(
				timeoutConnection, timeoutSocket));
		HttpPost httpPost = new HttpPost(url);
		HttpResponse httpResponse = null;
		HttpEntity httpEntity = null;

		JSONObject data = null;
		ByteArrayEntity baEntity = new ByteArrayEntity(json.toString()
				.getBytes(HTTP.UTF_8));
		baEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				ContentType.JSON.toString()));
		httpPost.setEntity(baEntity);
		httpResponse = httpClient.execute(httpPost);
		httpEntity = httpResponse.getEntity();

		int status = httpResponse.getStatusLine().getStatusCode();
		if (status == HttpStatus.SC_OK) {
			if (httpEntity != null) {
				data = InputStreamUtil.toJSON(httpEntity.getContent());
				EntityUtils.toString(httpEntity);
			} else {
				throw new HttpRequestException(ENTITY_NULL);
			}
		} else {
			throw new HttpRequestException(ERROR_HTTP + status);
		}
		listener.onResponseHttp(data);
	}

	@Override
	public Document getXML(String url) throws ClientProtocolException,
			HttpRequestException, IOException, ParserConfigurationException,
			SAXException {
		return InputStreamUtil.toDocumentXML(get(url));
	}

	@Override
	public String getString(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return InputStreamUtil.toString(get(url));
	}

	@Override
	public byte[] getContent(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return InputStreamUtil.toBytes(get(url));
	}
}
