package br.com.mirabilis.http;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import br.com.mirabilis.http.method.HttpMethod;
import br.com.mirabilis.http.parse.HttpParsedMethod;

/**
 * Class abstract that make request's using http protocol.
 * 
 * @author Rodrigo Sim�es Rosa.
 */
@SuppressWarnings("deprecation")
public abstract class HttpRequest implements HttpMethod, HttpParsedMethod {

	public static final String ERROR_HTTP = "Error http : ";
	public static final String ENTITY_NULL = "Objeto entity nulo";

	protected String cryptFormat;
	protected int timeoutConnection;
	protected int timeoutSocket;

	/**
	 * Boot block.
	 */
	{
		cryptFormat = HTTP.UTF_8;
		timeoutConnection = 0;
		timeoutSocket = 0;
	}

	/**
	 * Returns the parameters needed to perform the configuration.
	 * 
	 * @return
	 */
	protected HttpParams getHttpParams(int connection, int socket) {
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, connection);
		HttpConnectionParams.setSoTimeout(params, socket);
		return params;
	}

	/**
	 * Return list {@link NameValuePair}
	 * 
	 * @param map
	 * @return
	 */
	protected List<NameValuePair> getParams(Map<String, Object> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		Iterator<String> i = map.keySet().iterator();
		while (i.hasNext()) {
			String key = i.next();
			Object data = map.get(key);
			params.add(new BasicNameValuePair(key, String.valueOf(data)));
		}
		return params;
	}

	
}
