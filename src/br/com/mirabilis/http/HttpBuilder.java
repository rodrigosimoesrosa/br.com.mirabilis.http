package br.com.mirabilis.http;

import org.apache.http.HttpException;

import br.com.mirabilis.http.exception.HttpRequestException;
import br.com.mirabilis.http.info.HttpType;
import br.com.mirabilis.http.jakarta.HttpJakarta;
import br.com.mirabilis.http.java.HttpJava;

/**
 * Class that build objects http to request�s.
 * 
 * @author Rodrigo Sim�es Rosa
 * 
 */
public class HttpBuilder {
	/**
	 * Return request type.
	 * 
	 * @param context
	 * @param type
	 * @return
	 * @throws HttpException
	 */
	public static HttpRequest build(HttpType type) throws HttpRequestException {

		if (type == null) {
			return new HttpJakarta();
		}

		switch (type) {
		case JAVA:
			return new HttpJava();

		case JAKARTA:
			return new HttpJakarta();

		default:
			throw new HttpRequestException("This type of request not exist");
		}
	}

	/**
	 * Return request type.
	 * 
	 * @param context
	 * @return
	 * @throws HttpRequestException
	 */
	public static HttpRequest build() throws HttpRequestException {
		return build(null);
	}
}
