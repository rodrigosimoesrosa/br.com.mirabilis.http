package br.com.mirabilis.http.java;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import br.com.mirabilis.http.HttpRequest;
import br.com.mirabilis.http.exception.HttpRequestException;
import br.com.mirabilis.http.info.ContentType;
import br.com.mirabilis.http.info.HeaderType;
import br.com.mirabilis.http.info.HttpMethodType;
import br.com.mirabilis.http.listener.HttpRequestListener;
import br.com.mirabilis.http.util.InputStreamUtil;

/**
 * Implementation of {@link HttpRequest} using Jakarta lib's for request http.
 * 
 * @author Rodrigo Sim�es Rosa
 * 
 */
public final class HttpJava extends HttpRequest {

	/**
	 * Return {@link HttpURLConnection}
	 * 
	 * @param url
	 * @param method
	 * @return
	 * @throws IOException
	 */
	private HttpURLConnection getConnection(String url, HttpMethodType method)
			throws IOException {
		URL u = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) u.openConnection();
		connection.setDoInput(true);

		switch (method) {
		case GET:
			connection.setRequestMethod(HttpMethodType.GET.toString());
			connection.setDoOutput(false);
			break;

		case POST:
			connection.setRequestMethod(HttpMethodType.POST.toString());
			connection.setDoOutput(true);
		default:
			break;
		}

		return connection;
	}

	@Override
	public InputStream get(String url, int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, ClientProtocolException, IOException {
		HttpURLConnection connection = null;
		InputStream in = null;
		try {
			connection = getConnection(url, HttpMethodType.GET);
			connection.connect();
			int status = connection.getResponseCode();
			if (status == HttpStatus.SC_OK) {
				in = connection.getInputStream();
			} else {
				throw new HttpRequestException(ERROR_HTTP + status);
			}
		} finally {
			connection.disconnect();
		}
		return in;
	}

	@Override
	public InputStream get(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return get(url, this.timeoutConnection, this.timeoutSocket);
	}

	@Override
	public InputStream post(String url, Map<String, Object> map)
			throws ClientProtocolException, HttpRequestException, IOException {
		return post(url, map, this.timeoutConnection, this.timeoutSocket);
	}

	@Override
	public InputStream post(String url, Map<String, Object> map,
			int timeoutConnection, int timeoutSocket)
			throws UnsupportedEncodingException, HttpRequestException,
			ClientProtocolException, IOException {
		HttpURLConnection connection = null;
		InputStream in = null;
		try {
			connection = getConnection(url, HttpMethodType.POST);
			connection.connect();

			int status = connection.getResponseCode();
			if (status == HttpStatus.SC_OK) {
				OutputStream out = connection.getOutputStream();
				byte[] bytes = getQueryString(map).getBytes("UTF8");
				out.write(bytes);
				out.flush();
				out.close();
				in = connection.getInputStream();
			} else {
				throw new HttpRequestException(ERROR_HTTP + status);
			}

		} finally {
			connection.disconnect();
		}
		return in;
	}

	private String getQueryString(Map<String, Object> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}

		StringBuilder params = new StringBuilder();
		Iterator<String> i = (Iterator<String>) map.keySet().iterator();
		while (i.hasNext()) {
			String key = i.next();
			Object obj = map.get(key);
			String objValue = obj.toString();

			if (params.length() > 0) {
				params.append("&");
			}

			params.append(key.concat("=").concat(objValue));
		}

		return params.toString();
	}

	@Override
	public JSONObject getJson(String url) throws ClientProtocolException,
			HttpRequestException, IOException, JSONException {

		HttpURLConnection connection = null;
		InputStream in = null;
		JSONObject obj = null;

		try {
			connection = getConnection(url, HttpMethodType.GET);
			connection.setRequestProperty(ContentType.getString(),
					ContentType.JSON.toString());
			connection.setRequestProperty(HeaderType.ACCEPT.toString(),
					ContentType.JSON.toString());

			connection.connect();

			int status = connection.getResponseCode();
			if (status == HttpStatus.SC_OK) {
				in = connection.getInputStream();
				obj = InputStreamUtil.toJSON(in);
			} else {
				throw new HttpRequestException(ERROR_HTTP + status);
			}
		} finally {
			connection.disconnect();
		}

		return obj;
	}

	@Override
	public void postXML(String url, String xml,
			HttpRequestListener<String> listener)
			throws ClientProtocolException, HttpRequestException, IOException,
			ParserConfigurationException, SAXException {
		postXML(url, xml, listener, xml, timeoutConnection, timeoutSocket);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void postXML(String url, String xml,
			HttpRequestListener<String> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, UnsupportedEncodingException,
			ClientProtocolException, IOException, ParserConfigurationException,
			SAXException {

		HttpURLConnection connection = null;
		InputStream in = null;

		try {
			connection = getConnection(url, HttpMethodType.POST);
			connection.setRequestProperty(ContentType.getString(),
					ContentType.XML.toString());
			connection.setRequestProperty(HeaderType.ACCEPT.toString(),
					ContentType.XML.toString());

			connection.connect();

			int status = connection.getResponseCode();
			if (status == HttpStatus.SC_OK) {
				OutputStream out = connection.getOutputStream();
				byte[] bytes = xml.toString().getBytes(HTTP.UTF_8);
				out.write(bytes);
				out.flush();
				out.close();

				in = connection.getInputStream();
				listener.onResponseHttp(InputStreamUtil.toXML(in));
			}
		} finally {
			connection.disconnect();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void postJson(String url, JSONObject json,
			HttpRequestListener<JSONObject> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws UnsupportedEncodingException, ClientProtocolException,
			IOException, HttpRequestException, JSONException {

		HttpURLConnection connection = null;
		InputStream in = null;
		try {
			connection = getConnection(url, HttpMethodType.POST);
			connection.setRequestProperty(ContentType.getString(),
					ContentType.JSON.toString());
			connection.setRequestProperty(HeaderType.ACCEPT.toString(),
					ContentType.JSON.toString());

			connection.connect();

			int status = connection.getResponseCode();
			if (status == HttpStatus.SC_OK) {
				OutputStream out = connection.getOutputStream();
				byte[] bytes = json.toString().getBytes(HTTP.UTF_8);
				out.write(bytes);
				out.flush();
				out.close();

				in = connection.getInputStream();

				listener.onResponseHttp(InputStreamUtil.toJSON(in));

			} else {
				throw new HttpRequestException(ERROR_HTTP + status);
			}

		} finally {
			connection.disconnect();
		}
	}

	@Override
	public byte[] getContent(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return InputStreamUtil.toBytes(get(url));
	}

	@Override
	public Document getXML(String url) throws ClientProtocolException,
			HttpRequestException, IOException, ParserConfigurationException,
			SAXException {
		return InputStreamUtil.toDocumentXML(get(url));
	}

	@Override
	public String getString(String url) throws ClientProtocolException,
			HttpRequestException, IOException {
		return InputStreamUtil.toString(get(url));
	}
}
