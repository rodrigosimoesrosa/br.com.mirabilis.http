package br.com.mirabilis.http.info;

public enum HttpMethodType {

	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

}
