package br.com.mirabilis.http.info;

/**
 * Enumeration responsible for storing the types of data to be sent via POST.
 * 
 * @author Rodrigo Sim�es Rosa.
 */
public enum ContentType {
	XML("text/xml"), JSON("application/json"), XML_APP("application/xml");

	private String value;

	public static String getString() {
		return "Content-Type";
	}

	private ContentType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
}