package br.com.mirabilis.http.info;

/**
 * Enumeration armazer responsible for header types and their properties.
 * 
 * @author Rodrigo Sim�es Rosa.
 * 
 */
public enum HeaderType {
	ACCEPT("Accept");

	private String value;

	private HeaderType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
}