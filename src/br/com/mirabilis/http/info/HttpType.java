package br.com.mirabilis.http.info;

/**
 * Type of lib http.
 * 
 * @author Rodrigo Sim�es Rosa.
 * 
 */
public enum HttpType {
	JAVA, JAKARTA;
}