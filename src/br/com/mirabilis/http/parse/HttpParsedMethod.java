package br.com.mirabilis.http.parse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import br.com.mirabilis.http.exception.HttpRequestException;
import br.com.mirabilis.http.listener.HttpRequestListener;

/**
 * Interface that contains methods for parser objects acquired in http requests.
 * 
 * @author Rodrigo Sim�es Rosa.
 * 
 */
public interface HttpParsedMethod {

	/**
	 * Do download file.
	 * 
	 * @param url
	 *            to download file.
	 * @return {@link ResponseData} with array of bytes from image.
	 * @throws IOException
	 * @throws HttpRequestException
	 * @throws ClientProtocolException
	 */
	public byte[] getContent(String url) throws ClientProtocolException,
			HttpRequestException, IOException;

	/**
	 * Call http getJSON {@link JSONObject}
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws HttpRequestException
	 * @throws ClientProtocolException
	 * @throws JSONException
	 */
	public JSONObject getJson(String url) throws ClientProtocolException,
			HttpRequestException, IOException, JSONException;

	/**
	 * Call http getXML {@link Document}
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws HttpRequestException
	 * @throws ClientProtocolException
	 * @throws
	 */
	public Document getXML(String url) throws ClientProtocolException,
			HttpRequestException, IOException, ParserConfigurationException,
			SAXException;

	/**
	 * Call http getString {@link String}
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws HttpRequestException
	 * @throws ClientProtocolException
	 * @throws
	 */
	public String getString(String url) throws ClientProtocolException,
			HttpRequestException, IOException;

	/**
	 * Responsible for sending a post called XML.
	 * 
	 * @param url
	 *            address of request
	 * @param xml
	 *            content xml in string
	 * @param listener
	 *            to response data.
	 * @throws IOException
	 * @throws HttpRequestException
	 * @throws ClientProtocolException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public void postXML(String url, String xml,
			HttpRequestListener<String> listener)
			throws ClientProtocolException, HttpRequestException, IOException,
			ParserConfigurationException, SAXException;

	/**
	 * Responsible for sending a post called XML.
	 * 
	 * @param url
	 *            address of request.
	 * @param xml
	 *            content xml in string.
	 * @param listener
	 *            to response data.
	 * @param timeoutConnection
	 *            Timeout that will run until the connection established.
	 * @param timeoutSocket
	 *            Timeout will be the waiting time that the client waits.
	 * @throws HttpRequestException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public void postXML(String url, String xml,
			HttpRequestListener<String> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws HttpRequestException, UnsupportedEncodingException,
			ClientProtocolException, IOException, ParserConfigurationException,
			SAXException;

	/**
	 * The caller sending a Json post.
	 * 
	 * @param url
	 *            address of request.
	 * @param json
	 *            content json to send.
	 * @param listener
	 *            to response data.
	 * @param timeoutConnection
	 *            Timeout that will run until the connection established.
	 * @param timeoutSocket
	 *            Timeout will be the waiting time that the client waits.
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws HttpRequestException
	 * @throws JSONException
	 */
	public void postJson(String url, JSONObject json,
			HttpRequestListener<JSONObject> listener, String cryptFormat,
			int timeoutConnection, int timeoutSocket)
			throws UnsupportedEncodingException, ClientProtocolException,
			IOException, HttpRequestException, JSONException;

}
